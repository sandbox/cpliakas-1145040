<?php

/**
 * @file
 * Converts files using the Apache Tika library.
 */

class ConverterTika extends Converter {

  /**
   * For a file path, try to extract text using a local tika jar.
   */
  public function execute($filepath) {

    // @todo Make this a setting.
    $tika = '/opt/apache-tika-0.9/tika-app/target/tika-app-0.9.jar';

    // @todo Make this a setting.
    $java = 'java';

    // By default force UTF-8 output.
    $java_opts = ' ' . variable_get('converter_tika_java_opts', '-Dfile.encoding=UTF8');

    // Initialize the command.
    $cmd = escapeshellcmd($java . $java_opts) . ' -jar ' . escapeshellarg($tika) . ' -h ' . escapeshellarg($filepath);

    // Add a work-around for a MAMP bug + java 1.5.
    if (strpos(ini_get('extension_dir'), 'MAMP/')) {
      $cmd = 'export DYLD_LIBRARY_PATH=""; ' . $cmd;
    }

    exec($cmd, $output, $retvar);
    if ($retvar) {
      throw new Exception(t('An error occurred parsing the document.'));
    }

    // Returns everything inside the body tag.
    return $this->extractBody(join("\n", $output));
  }
}
