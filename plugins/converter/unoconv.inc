<?php

/**
 * @file
 * Unoconv converter plugin.
 */

class ConverterUnoconv extends Converter {

  /**
   * Converts the document.
   */
  function execute($filepath) {

    // Executes the command. A non-zero exit status means there was an error.
    // NOTE: There is some issue where you get html even if an exit code of
    // 139 is returned.
    $cmd = 'unoconv --stdout -f html ' . escapeshellarg($filepath);
    exec($cmd, $output, $retvar);
    if ($retvar) {
      throw new Exception(t('An error occurred parsing the document.'));
    }

    // Returns everything inside the body tag.
    return $this->extractBody(join("\n", $output));
  }

}
