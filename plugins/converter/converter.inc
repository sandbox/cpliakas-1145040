<?php

/**
 * @file
 * Base class for converter plugins.
 */

abstract class Converter {

  /**
   * Performs the conversion.
   *
   * @param string $filepath
   *   A string containing the path to the file.
   *
   * @throws Exception
   */
  abstract public function execute($filepath);

  /**
   * Helper method to extract the content inside of the body element.
   *
   * @param string $html
   *  The full HTML document the body is being extracted from.
   *
   * @return string
   *  The content inside of the body element, FALSE on errors.
   *
   * @throws Exception
   */
  public function extractBody($html) {
    if (preg_match_all('@<(body)[^>]*>(.*)</\1>@Uis', $html, $matches)) {
      return $matches[2][0];
    }
    else {
      throw new Exception(t('Error extracting the text inside the body element.'));
    }
  }
}
